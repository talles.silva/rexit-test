# Use an official PHP runtime as a parent image
FROM php:7.4-apache

 
WORKDIR /var/www/html/

 
COPY . .

 
RUN apt-get update && \
    apt-get install -y nano libpng-dev && \
    docker-php-ext-install mysqli gd 

RUN echo 'memory_limit = 20048M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini; 

RUN chmod 777 -R /var/www/html/
RUN chmod 777 -R /var/www/html/controllers
 
EXPOSE 80

 
CMD ["apache2-foreground"]
 
 
     
