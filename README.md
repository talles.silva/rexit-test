# RexIT-Test
This project was required as a task for the position PHP Developer at RexIT. It was developed in PHP 7.4, MySQL, Jquery/CS3.

## Deploy
To deploy the application and run it, please do the following steps:


1 - Execute the following command - 

```
sudo (if required) docker-compose up --build -d

```
2 - To populate the database, please execute the following steps:

```
  1 - sudo (if required) docker ps - get the containerID for the webserver 
  2 - sudo docker exec -it containerID bash 
  3 - php init.php - this will create database, tables and populate the table with the data that has been sent

```

```
3 - To execute the application please go to http://localhost:8000
4 - It will take a few seconds to populate the Datatable, after that all filters and the export button will be functional.

```
