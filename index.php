<?php

 
require('database/populate.php');

?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clients</title>


    <link href="css/tailwind.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dataTables.min.css" rel="stylesheet">
    <link href="css/buttons.dataTables.min.css" rel="stylesheet">


    <style>

  #clients_wrapper{
    width:100%;
  }

  #clients_length, #clients_paginate, #clients_info{
    padding:20px;
  }

    </style>
 </head>
 <body>

 <div class="w-full flex flex-col">

 <div class="w-full">

  <div style="padding: 30px;" class="w-full flex flex-col">
  <div class="p-3  w-ful flex flex-col">  
  <label for="category">Category</label>
  <select class="form-control" name="category" id="category">
    <option value="" >Select a category</option>
      <?php
    $conDB = conDatabase::getConnection();

    $sql = "select distinct category from client";
    $conDB->select_db('rexit');

    $result = $conDB->query($sql);
        
     if(!empty($result)){
      $arrayObj =[];

      while($rows = $result->fetch_assoc()){
        array_push($arrayObj, $rows);

      };

      foreach($arrayObj as $row){
        $option .= "<option value={$row['category']}>{$row['category']}</option>";
    
      }
      echo $option;
     }
      ?>
  

    </select>
    </div>
<div class="p-3  w-full flex flex-col">
<label for="gender">Gender</label>
    <select class="form-control" name="gender" id="gender">
        <option value="" >Select a gender</option>
        <option value="female">Female</option>
        
        <option value="male">Male</option>
    </select>
    </div>

    <div class="p-3  w-full flex flex-col">
      <label for="dateofbirth">Date of Birth</label>
    <input class="form-control" id="dateofbirth" type="text" name="datebitdh" placeholder="Date of Birth" value="">
    </div>

    <div class="p-3 w-full flex flex-col">
      <label for="age">Age</label>
    <input class="form-control" id="age" type="text" name="age" placeholder="Age" value="">
    </div>

    <div class="p-3  w-full flex flex-col">
      <div class="w-full flex flex-row"> 
          <div class="w-full flex flex-col mr-2">
              <label for="min">Age Range Initial</label>
                <input class="form-control" id="min" type="text" name="age-range-ini" placeholder="Age Range Initial" value="">
        </div>
            <div class="w-full flex flex-col">
                  <label for="max">Age Range Final</label>
                    <input class="form-control" id="max" type="text" name="age-range-fin" placeholder="Age Range Final" value="">
            </div>
          </div>
    </div>

    <div class="p-3 w-full">
      <form  action="controllers/export.php" method="get" id="export">
        <input  id="dataObj" name="data" hidden value="">
        <div class="w-full">
          <button class="w-full btn btn-success"id="export-btn">Export to CSV</button>
       </div>
    </form>
    </div>
</div>


</div>


   <table style="padding:20px" class="table" id="clients">
   <thead  class="text-white md:text-md\[20px]  text-gray-700 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                  <tr class="text-center">
                
                    <th id="category" style="text-align:center"   scope="col" class="text-center cursor-pointer ">Category</th>
                    <th id="firstname"  style="text-align:center" scope="col" class="text-center cursor-pointer">First Name</th>
                    <th id="lastname"  style="text-align:center" scope="col" class="text-center cursor-pointer ">Last Name</th>
                    <th id="email"  style="text-align:center" scope="col" class="text-center cursor-pointer">Email</th>
                    <th id="gender"   style="text-align:center" scope="col" class="text-center cursor-pointer ">Gender</th>
                    <th id="birthdate"  style="text-align:center" scope="col" class="text-center cursor-pointer ">Bith Date</th>           
                  </tr>
                </thead>


</table>




</div>
   


    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/TableCheckAll.js"></script>
    <script type="module" src="js/main.js"></script>
 </body>
 </html>