<?php 

require('database/con.php');

 
          
class PopulateDB{


    private $ObjFile;
  

    private function readFile(){
       
        try{

            $objFile = fopen("dataset.txt", "r") or die("Unable to open file!");

            $keys = fgetcsv($objFile, 0, "\t");

            $keys = explode(',', $keys[0]);
            
            $arrayObj = [];
   
 
        while (($line = fgetcsv($objFile , 0, "\t")) !== FALSE) { 
        $line = explode(',', $line[0]);  
         for($i=0;$i<count($keys); $i++){
            $line[$keys[$i]] = $line[$i];
            unset($line[$i]);
         }
    
             array_push($arrayObj, $line);        
            
          }
        

         return !empty($arrayObj) ? $arrayObj : [];
    }catch(Exception $e){
        die("Connection failed!");
    }

    }

    static public function populate(){

        $array = self::readFile();
 
        $conDB = conDatabase::init();
        $j = 0; 
        $arrayQuery =[];
        $query = "";
        
        if(!empty($array)){  
            

           for($i=0;$i<count($array);$i++){             
               
             $i=0;

             $query .= "INSERT INTO client (category, firstname, lastname,email, gender, birthdate) VALUES" ;  
            
             for($j=0;$j<2000;$j++){
                !empty($array[$j]) ? $query.= "('{$array[$j]['category']}', \"{$array[$j]['firstname']}\",\"{$array[$j]['lastname']}\",'{$array[$j]['email']}','{$array[$j]['gender']}','{$array[$j]['birthDate']}'),": ""; 
             } 
      
            
            $query .= ";";
        
            $query = str_replace(",;", ";", $query);
            $query = str_replace(";", ";\n", $query);
           
          
            for($j=0;$j<2000;$j++){
                unset($array[$j]);
            }
          
            $array = array_values($array);
           
          

            array_push($arrayQuery, $query);
           
            $query = "";           
                   
        }             
           
        try{
       
           
           $conDB->select_db("rexit");
            $conDB->begin_transaction();
            foreach($arrayQuery as $insertQuery){  
                try{
                   
                        if(!$stmt = $conDB->prepare($insertQuery)){
                            echo "Prepare failed: (" . $conDB->errno . ") " . $conDB->error;                           
                        }
                        $stmt->execute();
                        $conDB->commit();
                }catch (mysqli_sql_exception $exception) {
                    $conDB->rollback();
                    throw $exception;
                    }
              }           
                
                $conDB->close();
            } catch (mysqli_sql_exception $exception) {
                $conDB->rollback();
                throw $exception;
                }
        }else{
            throw new Exception('Failed to open the file');

        } 
    }
}

?>