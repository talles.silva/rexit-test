<?php

class conDatabase{
    
    static private $conDB;
       
    static public function createConnection(){

        try{
         
 
            $DB_CONNECTION="mysql";
            $DB_HOST="mysql-db";
            $DB_PORT="3306";
            $DB_DATABASE="rexit";
            $DB_USERNAME="dbadmin";
            $DB_PASSWORD="admin";
          
            self:self::$conDB = mysqli_connect($DB_HOST, $DB_USERNAME,$DB_PASSWORD);
         
            return self::$conDB;

        }catch (mysqli_sql_exception $exception) {
            throw $exception;
        }

    }   

    static public function getConnection(){
        global $con;
        if(self::$conDB){
            $con=self::$conDB;
            return $con;
        }
        else{
            $con=self::createConnection();
            return $con;	
        }
    }


    public static function init(){

        try{
                $dbCreate = "CREATE DATABASE IF NOT EXISTS rexit";

                $condb = self::getConnection();       
                

                    if ($condb->query($dbCreate)) {     
                        $createuser = "CREATE USER IF NOT EXISTS 'dbadmin'@'localhost';";
                        $setPassword = "SET PASSWORD FOR 'dbadmin'@'localhost' = 'admin'";
                        $setRole = "GRANT ALL PRIVILEGES ON rexit.* TO 'dbadmin'@'localhost' WITH GRANT OPTION;"   ;
                        
                        $condb->select_db('rexit');

                            $tableCreate = "CREATE  TABLE IF NOT EXISTS `client` (
                                `id` INT NOT NULL AUTO_INCREMENT,
                                `category` VARCHAR(250) NOT NULL,
                                `firstname` VARCHAR(250) NOT NULL,
                                `lastname` VARCHAR(250) NOT NULL,
                                `email` VARCHAR(45) NOT NULL,
                                `gender` VARCHAR(20) NOT NULL,
                                `birthdate` VARCHAR(15) NOT NULL,
                                PRIMARY KEY (`id`));" ;
            

                            if($condb->query($tableCreate)) {
                                $condb->query($createuser);
                                $condb->query($setPassword);
                                $condb->query($setRole);
                              
                                    return $condb;
                                   
                                }else{
                                    echo "Error creating table" . $conn->error;
                                }              
                            
                        
                        } else {
                            echo "Error creating database: " . $conn->error;
                        }
        } catch (mysqli_sql_exception $exception) {
            throw $exception;
        }

        }
}




  

?>