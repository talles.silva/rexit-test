import {fn} from "../js/functions.js"

$(document).ready(function(){

    
    fn.clean();

  var clients = $('#clients').DataTable({
        "dom":'Blrtip',
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bInfo": true,    
        "processing": true ,
        oLanguage: {sProcessing: "<div id='loader'></div>"},
        serverSide: false,
        buttons: [
            'csvHtml5'      
          ],
        "bAutoWidth": false,
       
        
        "search" : {
            "caseInsensitive": true,
             
        },
        
       
        "language": {          
            "zeroRecords": "No record found.",
            "infoEmpty": "Empty array"
        }, 
        "ajax" : {
            url : 'controllers/clients.php?q=true'        },
        columns: [
           
            {data:'category'},
            {data: 'firstname'},
            {data: 'lastname'},
            {data: 'email'},
            {data: 'gender'},
            {data: 'birthDate'},
            {data: 'age'},
        
        ],
        createdRow: (row) => {            
                $(row).addClass('border-b text-center');
        },
        "order": [],
        columnDefs: [
            { orderable: true, targets: 0 },
            { targets: [6], className: "hidden"},            
        ]
    
    })
    $('#category').change( function(e){
         return fn.filter(0,this.value)
    })

    $('#gender').change( function(e){
        return fn.filter(4,this.value)
   })

   $('#dateofbirth').off().on("keyup", function() {
      return fn.filter(5,this.value)
    })  

    $('#age').off().on("keyup", function() {
        return fn.filter(6,this.value)
      })  
  

      $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
        var min = parseInt($("#min").val(), 10 );
        var max = parseInt($('#max').val(), 10 );
        var age = parseFloat( data[6] ) || 0; // use data for the age column
     
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && age <= max ) ||
                 ( min <= age   && isNaN( max ) ) ||
                 ( min <= age   && age <= max ) )
            {
                return true;
            }
            return false;
        }
  
    )
     $('#min, #max').keyup( function() {
            clients.draw();
         } );


         $('#export-btn').click(function(e){

            var rows = clients.rows( {order:'index', search:'applied'} ).nodes();
            console.log(rows);
            var arrayObj = [];
            for(let i=0;i<rows.length;i++){

               arrayObj.push($(rows[i]).children());



            }
       
            var payLoad = [];
            for(var i=0; i<arrayObj.length;i++){
            

                for(var j=0;j<$(arrayObj[i]).length-1;j++){
                    
                    payLoad.push({"category": $(arrayObj[i][j]).text().replace("\n", "").trim(), 
                    "firstname" : $(arrayObj[i][+1]).text().replace("\n", "").trim(), 
                    "lastname" :  $(arrayObj[i][j+2]).text().replace("\n", "").trim(), 
                    "email" :  $(arrayObj[i][j+3]).text().replace("\n", "").trim(), 
                    "gender" :  $(arrayObj[i][j+4]).text().replace("\n", "").trim(), 
                    "birthdate" :  $(arrayObj[i][j+5]).text().replace("\n", "").trim()}  
                    ) 
                    j = j + 5;
                }

              
            }

            
                
         
       e.preventDefault();
           
    $.ajax({
        url: 'controllers/create.php',
        method: 'post',      
        data: {"data": payLoad} , 
         success: function(data){    
            var j = JSON.parse(data);
            if(j.success){
               $("#export").submit();
            }
        }
         
         })
})

})