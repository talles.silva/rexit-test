<?php
try{
  if(file_get_contents("dataset".date('dmY').".csv")){

      
        $file = "dataset".date('dmY').".csv";        
        header( 'Content-Type: text/csv' );
        header('Content-Description: File Transfer');
        header("Content-Disposition: attachment; filename=\"$file\""); 
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: '.filesize("dataset".date('dmY').".csv"));
        readfile("dataset".date('dmY').".csv");
        
  }else{
    throw new Exception('Failed to open the file');
  }
 
}catch (Exception $e) {
    echo "Failed to send the file " . $e->getMessage();
  }

?>